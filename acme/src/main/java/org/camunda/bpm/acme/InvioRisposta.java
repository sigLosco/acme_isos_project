package org.camunda.bpm.acme;

import java.util.logging.Logger;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.impl.util.json.JSONObject;

public class InvioRisposta implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger("ACCETTAZIONE PREVENTIVO");
    
	public void execute(DelegateExecution execution) throws Exception {	
		String appoggio= (String) execution.getVariable("ordine");
		String id_acme= (String) execution.getVariable("id_acme"); //id dell'istanza del processo acme
		JSONObject ordine = new JSONObject(appoggio);
		/*
		 * Settiamo la variabile con il valore inserito nella form valuta_preventivo.html
		 * che contiene l'accettazione o il rifiuto del preventivo presentato
		 * dall'amministrazione alla rivendita
		 */
		boolean esitoprev=(boolean)execution.getVariable("esitoprev"); 
		
		LOGGER.info("Il prezzo da versare e' : "+ ordine.get("prezzo"));
		/*
		 * Settiamo nell'esecuzione corrente l'esito del preventivo
		 */
		execution.setVariable("esitoprev", esitoprev);
		/*
		 * Comunichiamo all'azienda ACME la decisione presa sul preventivo presentato
		 */
		execution.getProcessEngineServices().getRuntimeService().createMessageCorrelation("MsgInvioRisp")
			.processInstanceId(id_acme)
			.setVariable("esitoprev", esitoprev)
			.correlate(); 
		/*
		 * Controlliamo l'esito del preventivo e stampiamo un messaggio 
		 * a seconda che sia stato accettato o rifiutato
		 */
		if(esitoprev) 
		{
			LOGGER.info("Il preventivo e' stato accettato dalla rivendita");
		}
		else
		{
			LOGGER.info("Il preventivo e' stato rifiutato dalla rivendita");
		}
	}
}
